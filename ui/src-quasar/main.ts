import Slicer from "./components/Slicer.vue";
import Table from "./components/Table.vue";
import Icon from "./components/Icon.vue";
// import Upload from "./components/Upload.vue";
import Tabs from "./components/Tabs.vue";
// import Affix from "./components/Affix.vue";
import Input from "./components/Input.vue";
// import NumberSlider from "./components/NumberSlider.vue";
import Drawer from "./components/Drawer.vue";
import Button from "./components/Button.vue";
import VisibleColumnsSlicer from "./components/VisibleColumnsSlicer.vue";
import Img from "./components/Img.vue";

export default [
  { tag: "qsSlicer", cp: Slicer },
  { tag: "qsTable", cp: Table },
  { tag: "Icon", cp: Icon },
  { tag: "qsTabs", cp: Tabs },
  { tag: "qsInput", cp: Input },
  { tag: "drawer", cp: Drawer },
  { tag: "qsButton", cp: Button },
  { tag: "VisibleColumnsSlicer", cp: VisibleColumnsSlicer },
  { tag: "Img", cp: Img },

  // { tag: 'Affix', cp: Affix },
  // { tag: 'qsNumberSlider', cp: NumberSlider },
];
