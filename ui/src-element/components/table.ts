import { type SqlValue } from "sql.js";
import { ComputedRef, Ref, onMounted } from "vue";


type TTableData = ComputedRef<{
    toRowsArrayHasHeader: () => Generator<SqlValue[], void, unknown>
}>

export function setupTableData2excel(boxRef: Ref<HTMLDivElement>, copyBtnText: Ref<string>, tableData: TTableData) {
    onMounted(() => {
        boxRef.value.addEventListener('click', (e) => {
            const lines = [];
            for (const row of tableData.value.toRowsArrayHasHeader()) {
                lines.push(row.join("\t"));
            }

            const excelData = lines.join("\n");

            navigator.clipboard.writeText(excelData).then(() => {

                copyBtnText.value = 'copied!'

                setTimeout(() => {
                    copyBtnText.value = 'copy'
                }, 2000);

            });
        });
    });
}




