import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from "path";
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'


// https://vitejs.dev/config/
export default defineConfig({


    plugins: [
        vue({
            template: { transformAssetUrls }
        }),
        quasar()
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },

    define: {
        'process.env': {}
    },

    publicDir: false,

    build: {
        cssCodeSplit: false,

        outDir: 'dist/quasar-libs',
        lib: {
            // Could also be a dictionary or array of multiple entry points
            entry: path.resolve(__dirname, 'src-quasar/main.ts'),
            fileName: 'quasarCps'
        },

        rollupOptions: {
            external: ['vue'],
            output: [{
                format: 'iife',
                name: 'quasarCps',
                globals: {
                    vue: "Vue"
                }
            }]
        }
    }




})
