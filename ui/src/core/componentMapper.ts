import { ComponentTag } from "@/models/component";
import { type Component } from "vue";
import Box from "@/components/Box.vue";
import FlowBox from "@/components/FlowBox.vue";
import GridBox from "@/components/GridBox.vue";
import TextValue from "@/components/TextValue.vue";
import Markdown from "@/components/Markdown.vue";
// import Icon from "@/components/Icon.vue";
// import SvgIcon from "@/components/SvgIcon.vue";
import Space from "@/components/Space.vue";
import Foreach from "@/components/Foreach.vue";
import Checkbox from "@/components/Checkbox.vue";


const mapping = new Map<string, Component>(
    [
        [ComponentTag.Box, Box],
        [ComponentTag.FlowBox, FlowBox],
        [ComponentTag.GridBox, GridBox],
        [ComponentTag.TextValue, TextValue],
        [ComponentTag.Markdown, Markdown],
        // [ComponentTag.Icon, Icon],
        // [ComponentTag.SvgIcon, SvgIcon],
        [ComponentTag.Space, Space],
        [ComponentTag.Foreach, Foreach],
        [ComponentTag.Checkbox, Checkbox],

    ]
)


export function getComponent(tag: ComponentTag) {
    return mapping.get(tag) ?? tag
}