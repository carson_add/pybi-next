import { TCpId } from "@/models/types";

export type TFilter = {
  cpid: TCpId;
  expression: string;
};

export interface IFiltersController {
  addFilter(cpid: TCpId, expression: string): void;
  removeFilter(cpid: TCpId): void;
  getAllFilters(): TFilter[];
}

export interface ISqlable {
  toSql(exclude: string[], noFilters: boolean): string;
  getName(): string;
}
