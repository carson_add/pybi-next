import { useEventListener } from "@vueuse/core";


export function scalePage() {

    function handler() {
        if (document.body.clientWidth < 1000 && document.body.clientWidth >= 300) {
            const value = 1 - ((document.body.clientWidth - 300) / 700)
            const scale = 1 - value * 0.7
            document.documentElement.style.fontSize = `${scale * 100}%`

            return
        }

        if (document.body.clientWidth < 300) {
            return
        }

        document.documentElement.style.fontSize = '100%'
    }

    useEventListener('resize', handler)
    handler()
}