import { createApp } from 'vue'
import './style.css'
import './style-tooltip.css'
import AppComponent from './AppLibs.vue'
import { App } from './models/app'
import { applyDirectives } from './directives'


function initApp(appConfig: App) {
    const app = createApp(AppComponent, { app: appConfig })
    applyDirectives(app)
    return app
}

export default {
    initApp
}
