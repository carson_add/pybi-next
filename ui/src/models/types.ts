


export type TDataSourceName = string
export type TCpId = string
export type TAppMetaInfos = {
    version: string
}

export type TForeachMapping = {
    field: string
}


export type TForeachRow = Record<string, any>