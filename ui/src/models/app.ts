import testingData from "@/tempData/configFromPy.json";
import { Container, DrawerContainer } from "./containers";
import { TDataSource, TDataView, TDataViewBase } from "./dataSources";
import { WebResource } from "@/services/webResourceServices";

export interface App extends Container {
  dataSources: TDataSource[];
  dataViews: TDataViewBase[];
  dbFile: string;
  dbLocalStorage: boolean;
  echartsRenderer: "canvas" | "svg";
  docTitle?: string;
  version: string;
  webResources: WebResource[];
  drawer: DrawerContainer;
  styleTags: {
    css: string;
    id?: string;
    media?: string;
  }[];
  imgResources: {
    id: string;
    bs64: string;
  }[];
}

export function getApp() {
  let config: App | null | string = null;

  if (import.meta.env.PROD) {
    config = "__{{__config_data__}}___";
  } else {
    config = testingData as App;
  }

  return config;
}
