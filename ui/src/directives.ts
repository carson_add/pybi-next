import { App, DirectiveBinding } from "vue";

type TTooltipArgs = { content: string, position: 'left' | 'right' | 'top' | 'bottom' }

export function applyDirectives(app: App) {
    app.directive('tooltip', {
        created: (el: HTMLElement, binding: DirectiveBinding<TTooltipArgs>, vnode, prevVnode) => {
            const tooltipTarget = document.querySelector('.pybi-tooltip') as HTMLElement

            el.addEventListener('mouseenter', () => {
                const rect = el.getBoundingClientRect()

                tooltipTarget?.setAttribute('position', binding.value.position)
                tooltipTarget?.setAttribute('data-pybi-tooltip', binding.value.content)

                tooltipTarget.style.top = rect.top + 'px'
                tooltipTarget.style.left = rect.left + 'px'
                tooltipTarget.style.width = rect.width + 'px'
                tooltipTarget.style.height = rect.height + 'px'
            })

            el.addEventListener('mouseleave', () => {
                tooltipTarget?.removeAttribute('data-pybi-tooltip')
            })
        },
    })
}