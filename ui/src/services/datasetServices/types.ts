
import { TCpId } from "@/models/types";
import { Ref } from "vue";



export interface IDataset {
    typeName: 'dataSource' | 'dataView' | 'pivot-dataView'
    name: string
    toSqlWithFilters: (requestorId: string, withOutFilters: boolean) => Ref<string>,
    addFilter: (updateId: TCpId, expression: string) => void,
    removeFilters: (updateId: TCpId) => void,

    initFilter: (updateId: TCpId) => void,
    getAllFilters(): {
        id: string
        filter: Ref<string>
    }[],
}


export interface IDataSource extends IDataset {

}

export interface IDataView extends IDataset {
    sql: string
    cp2FilterMap: Map<string, Ref<string>>
    addLinkageDataset: (dataset: IDataset) => void
}

export interface IPivotDataView extends IDataset {
    sourceDatasetName: string

    addLinkageDataset: (dataset: IDataset) => void
}