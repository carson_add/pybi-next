import { describe, it, expect } from "vitest";
import { getServices, replaceTableToFilterQuery } from "./sqlAnalyzeServices";


const service = getServices()


describe('extract single table name', () => {

    it('sample', () => {
        const act = service.getTableNames('select a,b,c from tableX')
        expect(act).toEqual(['tableX'])
    })

    it('sample upper letter', () => {
        const act = service.getTableNames('SELECT a,b,c FROM tableX')
        expect(act).toEqual(['tableX'])
    })

    it('sample chinese', () => {
        const act = service.getTableNames('SELECT 类别,名字,值 FROM tableX')
        expect(act).toEqual(['tableX'])
    })

    it('sample function', () => {
        const act = service.getTableNames('SELECT round(max(a),2) FROM tableX group by a')
        expect(act).toEqual(['tableX'])
    })



    it('with alias', () => {
        const act = service.getTableNames('SELECT round(max(a),2) as max_a FROM (select * from tableX where 1=1) as tabx group by a')
        expect(act).toEqual(['tableX'])
    })

    it('multiple lines', () => {
        const act = service.getTableNames(`
        select a, 
        round(avg(b),2) as fun1,
        round(avg(c),2) as fun2  
        from tab
        `)
        expect(act).toEqual(["tab"])
    })


})


describe('extract table name multiple queries', () => {

    it('sample', () => {
        const act = service.getTableNames('select a,b,c from tableX left join tabY on tableX.a=tabY.a')
        expect(act).toEqual(['tableX', 'tabY'])
    })

    it('subqueries', () => {
        const act = service.getTableNames('select a,b,c from (select * from tableX where 1=1) left join tabY on tableX.a=tabY.a')
        expect(act).toEqual(['tableX', 'tabY'])
    })

    it('with alias', () => {
        const act = service.getTableNames('select a,b,c from (select * from tableX where 1=1) as tabX left join tabY on tableX.a=tabY.a')
        expect(act).toEqual(['tableX', 'tabY'])
    })

    it('multiple lines', () => {
        const act = service.getTableNames(`
        select a,b,c 
        from (
            select * from 
            tableX where 1=1
        ) as tabX 
        left join tabY 
        on tableX.a=tabY.a
        `)
        expect(act).toEqual(['tableX', 'tabY'])
    })

})


describe('extract fields name for header', () => {

    it('sample', () => {
        const act = service.getFieldsForHeader('select a,b,c from tab')
        expect(act).toEqual(['a', 'b', 'c'])
    })

    it('with alias', () => {
        const act = service.getFieldsForHeader('select a as name1,b,c from (select x from tab1)')
        expect(act).toEqual(['name1', 'b', 'c'])
    })

    it('with func', () => {
        const act = service.getFieldsForHeader('select a, round(avg(b),2) as fun1,round(avg(c),2) as fun2  from tab')
        expect(act).toEqual(["a", "fun1", "fun2"])
    })

    it('multiple lines', () => {
        const act = service.getFieldsForHeader(`
        select a, 
        round(avg(b),2) as fun1,
        round(avg(c),2) as fun2  
        from tab
        `)
        expect(act).toEqual(["a", "fun1", "fun2"])
    })

    it('select *', () => {
        const act = service.getFieldsForHeader(`
        select *  
        from tab
        `)
        expect(act).toEqual(["*"])
    })

})



describe('table replace filters query', () => {

    it('should replace from', () => {
        const sql = 'select * from data'
        const targetTable = 'data'
        const filtersQuery = 'select a,b from data where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)


        expect(act).toBe('select * from (select a,b from data where a>10)')
    })

    it('should replace from multi', () => {
        const sql = `select * from ds1 where ds1.a=(select * from data where b="x1") and ds1.b=(select * from data where a=>100)`
        const targetTable = 'data'
        const filtersQuery = 'select a,b from data where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)

        expect(act).toBe(`select * from ds1 where ds1.a=(select * from (${filtersQuery}) where b="x1") and ds1.b=(select * from (${filtersQuery}) where a=>100)`)
    })


    it('should replace left join', () => {
        const sql = `select * from ds1 left join ds2`
        const targetTable = 'ds2'
        const filtersQuery = 'select * from ds2 where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)

        expect(act).toBe(`select * from ds1 left join (${filtersQuery})`)
    })

    it('should replace left join by upper', () => {
        const sql = `select * from ds1 LEFT OUTER Join ds2`
        const targetTable = 'ds2'
        const filtersQuery = 'select * from ds2 where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)

        expect(act).toBe(`select * from ds1 LEFT OUTER Join (${filtersQuery})`)
    })

    it('should replace left join by upper space', () => {
        const sql = `select * from ds1 LEFT    Join   Ds2`
        const targetTable = 'ds2'
        const filtersQuery = 'select * from ds2 where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)

        expect(act).toBe(`select * from ds1 LEFT    Join (${filtersQuery})`)
    })

    it('should replace right join by upper space', () => {
        const sql = `select * from ds1 Right    Join   Ds2`
        const targetTable = 'ds2'
        const filtersQuery = 'select * from ds2 where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)

        expect(act).toBe(`select * from ds1 Right    Join (${filtersQuery})`)
    })

    it('should replace inner join by upper space', () => {
        const sql = `select * from ds1 Inner    Join   Ds2`
        const targetTable = 'ds2'
        const filtersQuery = 'select * from ds2 where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)

        expect(act).toBe(`select * from ds1 Inner    Join (${filtersQuery})`)
    })

    it('should replace inner join without inner word', () => {
        const sql = `select * from ds1  Join   Ds2`
        const targetTable = 'ds2'
        const filtersQuery = 'select * from ds2 where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)

        expect(act).toBe(`select * from ds1  Join (${filtersQuery})`)
    })

    it('should replace cross join by upper space', () => {
        const sql = `select * from ds1 Cross    Join   Ds2`
        const targetTable = 'ds2'
        const filtersQuery = 'select * from ds2 where a>10'
        const act = replaceTableToFilterQuery(sql, targetTable, filtersQuery)

        expect(act).toBe(`select * from ds1 Cross    Join (${filtersQuery})`)
    })

})