import jszip from "jszip";


export function base64ToBytes(base64: string) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes;
}




export async function zipFile2Uint8Array(zipFile: Uint8Array) {
    const file = await jszip.loadAsync(zipFile)
    const key = Object.keys(file.files)[0]

    const res = file.files[key].async('uint8array')
    return res
}



export async function dbFile2bytes(fileBs64: string) {
    const zipFile = base64ToBytes(fileBs64)

    return await zipFile2Uint8Array(zipFile)
}