import { App } from "@/models/app";
import { getServices as getSqlServices } from "./sqlAnalyzeServices";
import { getServices as getComponentServices } from "./componentServices";
import { getServices as getDatasetServices } from "./datasetServices";
import { getServices as getDbServices } from "./dbServices";
import { getServices as getUtilsServices } from "./utilsServices";
import { getServices as getReactdataServices } from "./reactdataServices";
import { getServices as getWebResourceServices } from "./webResourceServices";
import { getServices as getActionsServices } from "./actionsServices";
import { getServices as getAppEventServices } from "./appEventServices";
import { getServices as getComponentInfosServices } from "./componentInfosServices";
import { getServices as getImgServices } from "./imgServices";
import wasm_base64 from "@/assets/wasm_base64.json";

export function initServices(app: App) {
  const webResourcesServices = getWebResourceServices(app);

  const dbServices = getDbServices(app, wasm_base64, webResourcesServices);

  const cpServices = getComponentServices(app);
  const sqlServices = getSqlServices();
  const datasetServices = getDatasetServices(app, {
    sqlAnalyze: sqlServices,
    component: cpServices,
    db: dbServices,
  });

  const utilsServices = getUtilsServices(
    datasetServices,
    sqlServices,
    dbServices
  );
  const reactdataServices = getReactdataServices({
    dataset: datasetServices,
    sqlAnalyze: sqlServices,
    db: dbServices,
    utils: utilsServices,
  });

  const actionsServices = getActionsServices();
  const appEventServices = getAppEventServices();
  const componentInfosServices = getComponentInfosServices();
  const imgServices = getImgServices(app);

  return {
    cpServices,
    sqlServices,
    datasetServices,
    dbServices,
    utilsServices,
    reactdataServices,
    webResourcesServices,
    actionsServices,
    appEventServices,
    componentInfosServices,
    imgServices,
  };
}
