import { TEChartOptions } from "@/models/reactiveComponent";
import { fixOptions as fixScatter } from "./scatterOptionsRefix";
import { fixOptions as fixLine } from "./lineOptionsRefix";
import { createOptionsOperator, createSeriesOperator } from "./utils";

export function refixOptions(options: TEChartOptions) {

    for (const series of options.series) {
        if (!('type' in series)) {
            continue
        }

        const optionsOperator = createOptionsOperator(options)
        const seriesOperator = createSeriesOperator(series)

        switch (series.type) {
            case 'line':
                fixLine(optionsOperator, seriesOperator)
                break;

            case 'bar':

                break;
            case 'pie':

                break;
            case 'scatter':
                fixScatter(optionsOperator, seriesOperator)
                break;
            default:
                break;
        }
    }
}