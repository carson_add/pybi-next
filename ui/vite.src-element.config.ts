

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from "path";

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'


// https://vitejs.dev/config/
export default defineConfig({


    plugins: [
        vue(),
        AutoImport({
            resolvers: [ElementPlusResolver()],
        }),
        Components({
            resolvers: [ElementPlusResolver()],
        }),
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },

    define: {
        'process.env': {}
    },

    publicDir: false,

    build: {
        // sourcemap: true,
        // target: 'esnext',
        // assetsInlineLimit: 488280,

        cssCodeSplit: false,

        outDir: 'dist/element-libs',
        lib: {
            // Could also be a dictionary or array of multiple entry points
            entry: path.resolve(__dirname, 'src-element/main.ts'),
            fileName: 'elementCps'
        },

        rollupOptions: {
            external: ['vue'],
            output: [{
                // file: '../pyvisflow/template/bundle.js',
                format: 'iife',
                name: 'elementCps',
                globals: {
                    vue: "Vue"
                }
            }]
        }
    }




})
