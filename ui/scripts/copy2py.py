from pathlib import Path
from shutil import copyfile

pj_name = "pybi"

cur_dir = Path(__file__).parent.absolute()
root = cur_dir.parent

dist_path = root / "dist" 
public_dir = root / "public"

dest_dir_path = root.parent / pj_name / "static" 

def copy2py(src_dir:str):
    src_dir = dist_path / src_dir
    js = next(src_dir.glob("*.iife.js"))
    core_name =  js.stem.split('.')[0]

    copyfile(js,dest_dir_path / js.name)

    styles = list(src_dir.glob("*.css"))
    if len(styles):
        copyfile(styles[0], dest_dir_path / f'{core_name}-style.css')

    
    



copy2py('app-libs')
copy2py('echarts-libs')
copy2py('element-libs')
copy2py('mermaid-libs')
copy2py('experimental-libs')
copy2py('quasar-libs')


print(f"done for bulid index.html. path:{dest_dir_path}")
