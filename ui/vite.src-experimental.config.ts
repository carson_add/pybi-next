import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from "path";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },

    define: {
        'process.env': {}
    },

    publicDir: false,

    build: {
        // sourcemap: true,
        // target: 'esnext',
        // assetsInlineLimit: 488280,

        cssCodeSplit: false,

        outDir: 'dist/experimental-libs',
        lib: {
            // Could also be a dictionary or array of multiple entry points
            entry: path.resolve(__dirname, 'src-experimental/main.ts'),
            fileName: 'experimentalCps'
        },

        rollupOptions: {
            external: ['vue'],
            output: [{
                // file: '../pyvisflow/template/bundle.js',
                format: 'iife',
                name: 'experimentalCps',
                globals: {
                    vue: "Vue"
                }
            }]
        }
    }




})
