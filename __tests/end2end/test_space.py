import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = True


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_data_display:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            [
                ["n1", 1, 10],
                ["n2", 2, 20],
                ["n3", 3, 30],
                ["n1", 4, 40],
                ["n4", 5, 50],
            ],
            columns=["name", "value1", "value2"],
        )

        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        data = pbi.set_source(data_df)

        with pbi.flowBox():
            pbi.add_slicer(data["name"])
            pbi.space()
            pbi.add_slicer(data["name"])
            pbi.add_slicer(data["name"])

        pbi.space()

        with pbi.flowBox():
            pbi.add_slicer(data["name"])
            pbi.add_slicer(data["name"])

        pbi.add_table(data)

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = utils.err_count(page)
        assert errs == 0
        # page.pause()
