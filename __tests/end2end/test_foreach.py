import _imports
import pytest
from playwright.sync_api import Page, sync_playwright
import pybi as pbi

import pandas as pd
from pathlib import Path
import utils


m_headless = False


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_data_display:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            {
                "name": [f"name{r}" for r in range(5)],
                "value": range(5),
            }
        )

        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        data = pbi.set_source(data_df)

        pbi.add_slicer(data["name"]).set_debugTag("name slicer")

        with pbi.foreach(f"select * from {data}") as row:
            pbi.add_text(row["name"])
            pbi.add_text(f'value:{row["value"]}')

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0
        # page.pause()

    def test_(self, page: Page):
        names = (f"name{r}" for r in range(4))
        # page.pause()

        pTags = page.locator("css = p[data-cp-tag='TextValue']")

        for name in names:
            assert pTags.filter(has_text=name).count() == 1

        values = (f"value:{r}" for r in range(4))

        for v in values:
            assert pTags.filter(has_text=v).count() == 1

        ps = utils.PageSlicer(page, "name slicer")

        ps.switch_options_pane()
        ps.select_options_by_text("name0")
        ps.select_options_by_text("name1")
        ps.select_options_by_text("name2")

        assert pTags.count() == 6

        ps.select_options_by_text("name1")

        assert pTags.count() == 4

        assert pTags.filter(has_text="name0").count() == 1
        assert pTags.filter(has_text="name2").count() == 1

        assert pTags.filter(has_text="value:0").count() == 1
        assert pTags.filter(has_text="value:2").count() == 1

        #
        assert pTags.filter(has_text="name1").count() == 0
        assert pTags.filter(has_text="value:1").count() == 0
