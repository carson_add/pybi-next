import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils
from typing import Dict


pbi.meta.set_echarts_renderer("svg")
charts = pbi.easy_echarts


@pytest.fixture(scope="module")
def page():

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=False)
        page = browser.new_page()
        yield page


class Test_bar_echarts:
    @pytest.fixture
    def file_url_base_bar(self):

        file = Path("test_result.html")

        df = pd.DataFrame(
            {
                "value": [120, 200, 150, 80, 70, 110, 130],
                "week": ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
            }
        )
        # 普通柱状图
        data = pbi.set_source(df)

        c = {
            "xAxis": {
                "type": "category",
                "data": pbi.sql(f"select week from {data}").toflatlist(),
            },
            "yAxis": {"type": "value"},
            "series": [
                {
                    "data": pbi.sql(f"select value from {data}").toflatlist(),
                    "type": "bar",
                    "showBackground": True,
                    "backgroundStyle": {"color": "rgba(180, 180, 180, 0.2)"},
                }
            ],
        }

        pbi.add_slicer(data["week"]).set_debugTag("week")
        pbi.add_echart(c).set_height("30em").set_debugTag("base")

        # data item style
        # https://echarts.apache.org/examples/zh/editor.html?c=bar-data-color
        series_data = pbi.sql(f"select value from {data}").js_map(
            """
        return rows.map(v=>{
            if (v.value<=100){
                return {value:v.value,itemStyle:{color:'#a90000'}}
            }
            return v.value
        })
        """
        )

        c = {
            "xAxis": {
                "type": "category",
                "data": pbi.sql(f"select week from {data}"),
            },
            "yAxis": {"type": "value"},
            "series": [
                {
                    "data": series_data,
                    "type": "bar",
                }
            ],
        }

        pbi.add_echart(c).set_height("30em").set_debugTag("item style")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def base_page(self, page: Page, file_url_base_bar):
        page.goto(file_url_base_bar)
        return page

    def test_should_no_errs(self, base_page: Page):
        assert utils.err_count(base_page) == 0

    def test_can_slicer_select(self, base_page: Page):
        page = base_page
        slicer = utils.PageSlicer(page, "week")
        slicer.switch_options_pane()
        slicer.select_options_by_text("Wed")
        slicer.select_options_by_text("Thu")
        assert utils.err_count(page) == 0

        chart = utils.PageEChart(page, "item style")
        opts = chart.get_options()
        data = opts["series"][0]["data"]
        assert len(data) == 2

        assert data[0] == 150
        assert data[1] == {"value": 80, "itemStyle": {"color": "#a90000"}}

        # assert utils.err_count(page) == 0
