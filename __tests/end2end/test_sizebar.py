import _imports
import pytest
from playwright.sync_api import Page, sync_playwright
import pybi as pbi

import pandas as pd
from pathlib import Path
import utils


m_headless = False


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


# @pytest.mark.skip("todo")
class Test_data_display:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            {
                "name": [f"name{r}" for r in range(5)],
                "value": range(5),
            }
        )

        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        data = pbi.set_source(data_df)

        with pbi.drawer(value=False) as drawer:
            pbi.add_slicer(data["name"]).set_debugTag("name slicer")

        btn_switch = (
            pbi.add_button("switch show")
            .bind_action(drawer.actions.switch_show)
            .set_debugTag("switch show")
        )

        pbi.add_table(data).set_debugTag("table1")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_(self, page: Page):
        assert page.locator("css = .pybi-drawer").count() == 1

        # sizebar = utils.PageSizebar(page)
        # sizebar.open()
        btn_switch = utils.PageButton(page, "switch show")
        btn_switch.click()

        ns = utils.PageSlicer(page, "name slicer")
        ns.switch_options_pane()
        ns.select_options_by_text("name1")
        ns.select_options_by_text("name3")

        btn_switch.click()

        table = utils.PageTable(page, "table1")
        assert table.get_rows().count() == 2

        assert table.get_row_values(0) == ["name1", "1"]
        assert table.get_row_values(1) == ["name3", "3"]

        # page.pause()
