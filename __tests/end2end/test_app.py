import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils
import os


m_headless = "GITHUB_ACTION" in os.environ


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_actions:
    @pytest.fixture
    def file_url(self):
        file = Path("test_result.html")

        df1 = pd.DataFrame({"name": [f"n{i}" for i in range(10)], "value": range(10)})
        ds1 = pbi.set_source(df1)

        df2 = pd.DataFrame({"name": [f"xxx{i}" for i in range(10)], "value": range(10)})
        ds2 = pbi.set_source(df2)

        pbi.add_button("清除筛选").bind_action(pbi.actions.reset_filters).set_debugTag(
            "btn"
        )

        pbi.add_slicer(ds1["name"]).set_debugTag("ds1 name slicer")
        pbi.add_checkbox(ds1["name"]).set_debugTag("ds1 name checkbox")
        pbi.add_table(ds1).set_debugTag("ds1 table")

        pbi.add_slicer(ds2["name"]).set_debugTag("ds2 name slicer")
        pbi.add_table(ds2).set_debugTag("ds2 table")

        pbi.add_echart(
            pbi.easy_echarts.make_bar(ds1, x="name", y="value").click_filter(
                "x", ds1, "name"
            )
        ).set_debugTag("chart")

        tabs = pbi.add_tabs(["a", "b"]).set_debugTag("tabs")

        with tabs["a"]:
            pbi.add_input(ds1["name"]).set_debugTag("input")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_no_error(self, page: Page):
        errs = page.locator("css=.err").count()
        assert errs == 0

    def test_reset_filter(self, page: Page):
        slicer1 = utils.PageSlicer(page, "ds1 name slicer")
        checkbox1 = utils.PageCheckbox(page, "ds1 name checkbox")
        table1 = utils.PageTable(page, "ds1 table")

        slicer2 = utils.PageSlicer(page, "ds2 name slicer")
        table2 = utils.PageTable(page, "ds2 table")

        chart = utils.PageEChart(page, "chart")

        tabs = utils.PageTabs(page, "tabs")
        input = utils.PageInput(page, "input")

        reset_filter_btn = utils.PageButton(page, "btn")

        slicer1.switch_options_pane()

        slicer1.select_options_by_text("n1")
        slicer1.select_options_by_text("n2")

        slicer1.switch_options_pane()

        checkbox1.click_by_text("n2")

        # slicer2
        slicer2.switch_options_pane()

        slicer2.select_options_by_text("xxx1")
        slicer2.select_options_by_text("xxx2")
        slicer2.select_options_by_text("xxx4")

        slicer2.switch_options_pane()

        assert table1.get_rows().count() == 1
        assert table2.get_rows().count() == 3

        reset_filter_btn.click()

        assert table1.get_rows().count() == 10
        assert table2.get_rows().count() == 10

        # test chart click
        chart.scroll2show().click_series("n4", 4)

        assert table1.get_rows().count() == 1

        reset_filter_btn.click()

        assert table1.get_rows().count() == 10

        # test input in tabs
        input.input("n1")
        assert table1.get_rows().count() == 1

        # change to page b and back to page a
        tabs.activate_tab("b")
        tabs.activate_tab("a")

        reset_filter_btn.click()

        assert table1.get_rows().count() == 10

        # page.pause()
