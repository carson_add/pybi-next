import _imports
import pytest
from playwright.sync_api import Page, sync_playwright
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

m_headless = False


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=m_headless)
        page = browser.new_page()
        yield page


class Test_tabs:
    @pytest.fixture
    def file_url(self):
        file = Path("test_result.html")

        tab1, tab2 = pbi.add_tabs(["page1", "page2"]).set_debugTag("tabs1")
        with tab1:
            pbi.add_text("tab1")

        with tab2:
            pbi.add_text("tab2")

        tabs = pbi.add_tabs(["page1", "page2"]).set_debugTag("tabs by key")
        with tabs["page1"]:
            pbi.add_text("tabs by key 1")

        with tabs["page2"]:
            pbi.add_text("tabs by key 2")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_no_errs(self, page: Page):
        # page.pause()
        assert utils.err_count(page) == 0

    def test_change_tabs(self, page: Page):
        tabs1 = utils.PageTabs(page, "tabs1")

        text = tabs1.get_body_text()
        assert text == "tab1"

        tabs1.activate_tab("page2")
        # page.pause()
        text = tabs1.get_body_text()
        assert text == "tab2"

        # tabs by key
        tabs_keys = utils.PageTabs(page, "tabs by key")

        text = tabs_keys.get_body_text()
        assert text == "tabs by key 1"

        tabs_keys.activate_tab("page2")
        text = tabs_keys.get_body_text()
        assert text == "tabs by key 2"

        # page.pause()
        # assert utils.err_count(page) == 0
