import _imports
import pytest
from playwright.sync_api import Page, sync_playwright, expect
import pybi as pbi
import pandas as pd
from pathlib import Path
import utils

pbi.meta.set_echarts_renderer("svg")
charts = pbi.easy_echarts


@pytest.fixture(scope="module")
def page():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=False)
        page = browser.new_page()
        yield page


class Test_Slicer_effect:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            [
                ["广东省", "广州市", "荔湾区"],
                ["广东省", "广州市", "海珠区"],
                ["广东省", "广州市", "白云区"],
                ["广东省", "深圳市", "南山区"],
                ["广东省", "深圳市", "盐田区"],
                ["广东省", "深圳市", "福田区"],
                ["湖南省", "长沙市", "芙蓉区"],
                ["湖南省", "长沙市", "天心区"],
                ["湖南省", "株洲市", "石峰"],
                ["湖南省", "株洲市", "渌口"],
            ],
            columns=list("省市区"),
        )

        df["value"] = range(1, len(df) + 1)
        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        file = Path("test_result.html")

        data = pbi.set_source(data_df)
        dv = pbi.set_dataView(f"select 省,sum(value) as total from {data} group by 省")

        grid = """
            s-left   chart
            s-left tab-right
        """

        with pbi.gridBox(grid):
            with pbi.flowBox().set_gridArea("s-left"):
                for name in "省市区":
                    pbi.add_slicer(data[name]).set_debugTag(name)

                pbi.add_table(data).set_debugTag("tab-left")

            pbi.add_table(dv).set_debugTag("tab-right").set_gridArea("tab-right")

            opts = charts.make_bar(dv, x="省", y="total")
            pbi.add_echart(opts).set_debugTag("chart1").hover_filter(
                "x", data, "省"
            ).set_gridArea("chart")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_should_slicer_effect_chart(self, page: Page):
        chart = utils.PageEChart(page, "chart1")

        opts = chart.get_options()

        series_data = opts["series"][0]["data"]
        assert len(series_data) == 2

        exp_data_values = [21, 34]

        for itemData, value in zip(series_data, exp_data_values):
            if isinstance(itemData, dict):
                assert "value" in itemData
                assert itemData["value"] == value
            else:
                assert itemData == value

        assert opts["xAxis"][0]["type"] == "category"
        assert opts["xAxis"][0]["data"] == ["广东省", "湖南省"]

        # slicer selected
        slicer_pvc = utils.PageSlicer(page, "省")
        slicer_pvc.switch_options_pane()
        slicer_pvc.select_options_by_text("湖南省")

        opts = chart.get_options()
        assert opts["xAxis"][0]["data"] == ["湖南省"]

        series_data = opts["series"][0]["data"]
        assert len(series_data) == 1

    def test_should_chart_only_effect_left_table(self, page: Page):
        table_left = utils.PageTable(page, "tab-left")
        table_right = utils.PageTable(page, "tab-right")

        chart = utils.PageEChart(page, "chart1").scroll2show()

        chart.hover_series("广东省", 15)
        # page.pause()
        assert table_left.get_rows().count() == 6

        assert table_left.get_table_col_values(-1) == list(map(str, range(1, 7)))

        assert table_right.get_rows().count() == 1
        assert table_right.get_row_values(0) == ["广东省", "21"]

        # 湖南省
        chart.hover_series("湖南省", 20)

        assert table_left.get_rows().count() == 4
        assert table_left.get_table_col_values(-1) == list(map(str, range(7, 11)))

        assert table_right.get_rows().count() == 1
        assert table_right.get_row_values(0) == ["湖南省", "34"]


class Test_all_echarts:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            [
                ["广东省", "广州市", "荔湾区"],
                ["广东省", "广州市", "海珠区"],
                ["广东省", "广州市", "白云区"],
                ["广东省", "深圳市", "南山区"],
                ["广东省", "深圳市", "盐田区"],
                ["广东省", "深圳市", "福田区"],
                ["湖南省", "长沙市", "芙蓉区"],
                ["湖南省", "长沙市", "天心区"],
                ["湖南省", "株洲市", "石峰"],
                ["湖南省", "株洲市", "渌口"],
            ],
            columns=list("省市区"),
        )

        df["value"] = range(1, len(df) + 1)
        return df

    @pytest.fixture
    def data_df_k(self):
        df = pd.DataFrame(
            [
                ["2017-10-24", 20, 34, 10, 38],
                ["2017-10-25", 40, 35, 30, 50],
                ["2017-10-26", 31, 38, 33, 44],
                ["2017-10-27", 38, 15, 5, 42],
            ],
            columns=["date", "open", "close", "lowest", "highest"],
        )

        return df

    @pytest.fixture
    def data_df_radar(self):
        df = pd.DataFrame(
            {
                "indicator": ["A", "B", "C", "D", "E", "F", "A", "B", "C", "D", "F"],
                "value": [
                    8150,
                    1035,
                    3393,
                    7919,
                    4244,
                    6237,
                    3537,
                    5676,
                    8306,
                    6388,
                    3889,
                ],
                "name": ["X", "X", "X", "X", "X", "X", "Y", "Y", "Y", "Y", "Y"],
            }
        )

        return df

    @pytest.fixture
    def file_url(
        self,
        data_df: pd.DataFrame,
        data_df_k: pd.DataFrame,
        data_df_radar: pd.DataFrame,
    ):
        file = Path("test_result.html")

        data = pbi.set_source(data_df)
        data_k = pbi.set_source(data_df_k)
        data_radar = pbi.set_source(data_df_radar)

        opts = charts.make_bar(data, x="省", y="value")
        pbi.add_echart(opts)

        opts = charts.make_bar(data, x="省", y="value").reverse_axis()
        pbi.add_echart(opts)

        opts = charts.make_bar(data, x="省", y="value", color="市")
        pbi.add_echart(opts)

        opts = charts.make_bar(data, x="省", y="value", color="市").reverse_axis()
        pbi.add_echart(opts)

        # line
        opts = charts.make_line(data, x="省", y="value")
        pbi.add_echart(opts)

        opts = charts.make_line(data, x="省", y="value", color="市")
        pbi.add_echart(opts)

        # pie
        opts = charts.make_pie(data, name="省", value="value")
        pbi.add_echart(opts)

        opts = charts.make_pie(data, name="省", value="value", agg="sum")
        pbi.add_echart(opts)

        # scatter
        opts = charts.make_scatter(data, x="省", y="value")
        pbi.add_echart(opts).set_debugTag("scatter xy")

        opts = charts.make_scatter(data, x="value", y="省", color="市")
        pbi.add_echart(opts).set_debugTag("scatter color")

        # drill down
        opts_bar = charts.make_bar(data, x="省", y="value", color="市").reverse_axis()
        opts_pie = opts = charts.make_pie(data, name="省", value="value")
        opts_line = charts.make_line(data, x="区", y="value")

        opts = opts_bar + opts_pie + opts_line

        pbi.add_echart(opts)

        # drill down one series
        opts_bar = charts.make_bar(data, x="省", y="value").reverse_axis()
        opts_pie = opts = charts.make_pie(data, name="省", value="value")
        opts_line = charts.make_line(data, x="区", y="value")

        opts = opts_bar + opts_pie + opts_line

        pbi.add_echart(opts).set_debugTag("drill")

        # Error:id duplicates: x
        opts = charts.make_bar(data, x="省", y="value")
        pbi.add_echart(opts)
        pbi.add_echart(opts.reverse_axis())

        # map
        opts = charts.make_map()
        pbi.add_echart(opts)

        #
        opts = charts.make_candleStick(data_k)
        pbi.add_echart(opts).set_height("30em")

        # radar
        opts = charts.make_radar(data_radar)
        pbi.add_echart(opts).set_height("30em").set_debugTag("radar")

        #
        with pbi.gridBox(
            """
            chart1
        """
        ):
            pbi.add_echart(
                {
                    "xAxis": {
                        "type": "category",
                        "data": ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                    },
                    "yAxis": {"type": "value"},
                    "series": [
                        {"data": [150, 230, 224, 218, 135, 147, 260], "type": "line"}
                    ],
                }
            ).set_gridArea("chart1").set_style("height:200px").set_debugTag(
                "scrollbar test"
            )

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_no_err(self, page: Page):
        errs = utils.err_count(page)
        assert errs == 0

        # page.pause()

    def test_scatter_opts_no_err(self, page: Page):
        chart = utils.PageEChart(page, "scatter xy").scroll2show()

        opts = chart.get_options()
        assert opts["xAxis"][0]["type"] == "category"
        assert opts["yAxis"][0]["type"] != "category"

        exp = [
            ["广东省", 1],
            ["广东省", 2],
            ["广东省", 3],
            ["广东省", 4],
            ["广东省", 5],
            ["广东省", 6],
            ["湖南省", 7],
            ["湖南省", 8],
            ["湖南省", 9],
            ["湖南省", 10],
        ]

        series = opts["series"][0]
        assert series["data"] == exp
        assert series["type"] == "scatter"

        # scatter color
        chart = utils.PageEChart(page, "scatter color").scroll2show()

        opts = chart.get_options()

        allSeries = opts["series"]
        assert len(allSeries) == 4

        data_exps = [
            [[1, "广东省"], [2, "广东省"], [3, "广东省"]],
            [[4, "广东省"], [5, "广东省"], [6, "广东省"]],
            [[7, "湖南省"], [8, "湖南省"]],
            [[9, "湖南省"], [10, "湖南省"]],
        ]

        for series, data_exp in zip(allSeries, data_exps):
            assert series["data"] == data_exp
            assert series["type"] == "scatter"

            assert opts["xAxis"][0]["type"] != "category"
            assert opts["yAxis"][0]["type"] == "category"

    def test_radar_opts_no_err(self, page: Page):
        chart = utils.PageEChart(page, "radar").scroll2show()

        opts = chart.get_options()
        series = opts["series"][0]

        assert series["type"] == "radar"
        assert len(series["data"]) == 2

        exp_series_data = [
            {"name": "X", "value": [8150, 1035, 3393, 7919, 4244, 6237]},
            {"name": "Y", "value": [3537, 5676, 8306, 6388, None, 3889]},
        ]

        assert series["data"] == exp_series_data

        # indicator
        exp_indicator_data = [
            {"name": "A", "max": 8150, "min": 0},
            {"name": "B", "max": 5676, "min": 0},
            {"name": "C", "max": 8306, "min": 0},
            {"name": "D", "max": 7919, "min": 0},
            {"name": "E", "max": 4244, "min": 0},
            {"name": "F", "max": 6237, "min": 0},
        ]

        assert opts["radar"][0]["indicator"] == exp_indicator_data

    def test_should_not_present_scrollbar(self, page: Page):
        chart = utils.PageEChart(page, "scrollbar test").scroll2show()
        assert not chart.has_scrollbar()


class Test_echarts_data:
    @pytest.fixture
    def file_url(self):
        df = pd.DataFrame(
            [
                ["catA", "x1", 100],
                ["catA", "x3", 200],
                ["catB", "x1", 1000],
                ["catB", "x2", 2000],
                ["catB", "x3", 3000],
            ],
            columns=["cat", "name", "value"],
        )

        data = pbi.set_source(df)
        opts = charts.make_bar(data, x="name", y="value", color="cat", agg="sum")
        pbi.add_echart(opts).set_debugTag("bar")

        opts = charts.make_line(data, x="name", y="value", color="cat", agg="sum")
        pbi.add_echart(opts).set_debugTag("line")

        data = pbi.set_source(df)
        opts = charts.make_bar(
            data, x="name", y="value", color="cat", agg="sum"
        ).reverse_axis()
        pbi.add_echart(opts).set_debugTag("bar reverse_axis")

        def line_by_number_x_value():
            df = pd.DataFrame(
                [[1, "cat1", 100], [2, "cat1", 200], [3, "cat1", 1000]],
                columns=["x", "cat", "y"],
            )

            ds = pbi.set_source(df)

            opts = charts.make_line(ds, x="x", y="y", color="cat")
            pbi.add_echart(opts).set_debugTag("line xaxis number value")

        line_by_number_x_value()

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_no_err(self, page: Page):
        errs = utils.err_count(page)
        assert errs == 0

        # page.pause()

    def test_bar_by_color(self, page: Page):
        bar = utils.PageEChart(page, "bar")
        opts = bar.get_options()

        assert len(opts["series"]) == 2
        assert opts["series"][0]["data"] == [["x1", 100], ["x3", 200]]
        assert opts["series"][1]["data"] == [["x1", 1000], ["x2", 2000], ["x3", 3000]]

        bar = utils.PageEChart(page, "bar reverse_axis")
        opts = bar.get_options()

        assert len(opts["series"]) == 2
        assert opts["yAxis"][0]["data"] == ["x1", "x2", "x3"]
        assert opts["series"][0]["data"] == [[100, "x1"], [200, "x3"]]
        assert opts["series"][1]["data"] == [[1000, "x1"], [2000, "x2"], [3000, "x3"]]

    def test_line_by_color(self, page: Page):
        line = utils.PageEChart(page, "line")
        opts = line.get_options()

        assert len(opts["series"]) == 2
        assert opts["series"][0]["data"] == [["x1", 100], ["x3", 200]]
        assert opts["series"][1]["data"] == [["x1", 1000], ["x2", 2000], ["x3", 3000]]

    def test_line_by_xaxis_number(self, page: Page):
        line = utils.PageEChart(page, "line xaxis number value")
        opts = line.get_options()

        assert opts["xAxis"][0]["type"] == "value"

    def test_has_tooltip_base(self, page: Page):
        line = utils.PageEChart(page, "line")
        opts = line.get_options()

        assert "tooltip" in opts
        assert (
            isinstance(opts["tooltip"], list) and len(opts["tooltip"]) > 0
        ), "tooltip not a empty list"


class Test_echarts_merge:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            [
                ["广东省", "广州市", "荔湾区"],
                ["广东省", "广州市", "海珠区"],
                ["广东省", "广州市", "白云区"],
                ["广东省", "深圳市", "南山区"],
                ["广东省", "深圳市", "盐田区"],
                ["广东省", "深圳市", "福田区"],
                ["湖南省", "长沙市", "芙蓉区"],
                ["湖南省", "长沙市", "天心区"],
                ["湖南省", "株洲市", "石峰"],
                ["湖南省", "株洲市", "渌口"],
            ],
            columns=list("省市区"),
        )

        df["value"] = range(1, len(df) + 1)
        return df

    @pytest.fixture
    def data_df_k(self):
        df = pd.DataFrame(
            [
                ["2017-10-24", 20, 34, 10, 38],
                ["2017-10-25", 40, 35, 30, 50],
                ["2017-10-26", 31, 38, 33, 44],
                ["2017-10-27", 38, 15, 5, 42],
            ],
            columns=["date", "open", "close", "lowest", "highest"],
        )

        return df

    @pytest.fixture
    def data_df_radar(self):
        df = pd.DataFrame(
            {
                "indicator": ["A", "B", "C", "D", "E", "F", "A", "B", "C", "D", "F"],
                "value": [
                    8150,
                    1035,
                    3393,
                    7919,
                    4244,
                    6237,
                    3537,
                    5676,
                    8306,
                    6388,
                    3889,
                ],
                "name": ["X", "X", "X", "X", "X", "X", "Y", "Y", "Y", "Y", "Y"],
            }
        )

        return df

    @pytest.fixture
    def data_df_series_merge(self):
        df = pd.DataFrame(
            [
                {"月": "3", "类型": "x1", "值": 100},
                {"月": "4", "类型": "x1", "值": 200},
                {"月": "5", "类型": "x1", "值": 300},
                {"月": "3", "类型": "x2", "值": 50},
                {"月": "4", "类型": "x2", "值": 100},
                {"月": "5", "类型": "x2", "值": 250},
                {"月": "3", "类型": "x3", "值": 400},
                {"月": "4", "类型": "x3", "值": 450},
                {"月": "5", "类型": "x3", "值": 500},
            ]
        )

        return df

    @pytest.fixture
    def file_url(
        self,
        data_df: pd.DataFrame,
        data_df_k: pd.DataFrame,
        data_df_radar: pd.DataFrame,
        data_df_series_merge: pd.DataFrame,
    ):
        file = Path("test_result.html")

        data = pbi.set_source(data_df)
        data_k = pbi.set_source(data_df_k)
        data_radar = pbi.set_source(data_df_radar)
        data_series_merge = pbi.set_source(data_df_series_merge)

        opts = charts.make_bar(data, x="省", y="value").merge({"title": {"text": "xxx"}})
        pbi.add_echart(opts)

        # line
        opts = charts.make_line(data, x="省", y="value").merge(
            {
                "title": {"text": "xxx"},
                "series": {
                    "label": {"show": True},
                },
            }
        )
        pbi.add_echart(opts)

        # line chart show data label
        pbi.add_slicer(data_series_merge["类型"]).set_debugTag(
            "slicer for line chart show data label"
        )

        opts = charts.make_line(data_series_merge, x="月", y="值", color="类型").merge(
            {
                "title": {"text": "xxx"},
                "series": {
                    "label": {"show": True},
                },
            }
        )
        pbi.add_echart(opts).set_debugTag("line chart series label")

        # pie
        opts = charts.make_pie(data, name="省", value="value").merge(
            {"title": {"text": "xxx"}}
        )
        pbi.add_echart(opts)

        # scatter
        opts = charts.make_scatter(data, x="省", y="value").merge(
            {"title": {"text": "xxx"}}
        )
        pbi.add_echart(opts).set_debugTag("scatter xy")

        # map
        opts = charts.make_map().merge({"title": {"text": "xxx"}})
        pbi.add_echart(opts)

        #
        opts = charts.make_candleStick(data_k).merge({"title": {"text": "xxx"}})
        pbi.add_echart(opts).set_height("30em")

        # radar
        opts = charts.make_radar(data_radar).merge({"title": {"text": "xxx"}})
        pbi.add_echart(opts).set_height("30em").set_debugTag("radar")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_title_setting_merge(self, page: Page):
        errs = utils.err_count(page)
        assert errs == 0

        # page.pause()
        charts_ins = page.locator('css=[data-cp-tag="EChart"] > .echart').all()

        opts: list[dict] = [
            ins.evaluate("node => echarts.getInstanceByDom(node).getOption()")
            for ins in charts_ins
        ]

        for opt in opts:
            assert "title" in opt

            title_opt = opt["title"][0]
            assert "text" in title_opt
            assert title_opt["text"] == "xxx"

    def test_series_setting_merge(self, page: Page):
        # chart line data label show
        line_chart = utils.PageEChart(page, "line chart series label").scroll2show()
        opts = line_chart.get_options()
        for s in opts["series"]:
            assert s["label"]["show"] == True

        # when filter dataset
        slicer = utils.PageSlicer(page, "slicer for line chart show data label")
        slicer.switch_options_pane()
        slicer.select_options_by_text("x1")

        opts = line_chart.get_options()
        for s in opts["series"]:
            assert s["label"]["show"] == True


class Test_hover_filter:
    @pytest.fixture
    def file_url_one_series(self):
        df = pd.DataFrame(
            [
                ["信息化", "IT运营部", 128.6201, 121, 12],
                ["信息化", "IT运营部", 128.6201, 116, 11],
                ["信息化", "IT运营部", 128.6201, 24, 10],
                ["信息化", "IT运营部", 128.6201, 12, 9],
                ["信息化", "IT运营部", 128.6201, 22, 8],
                ["信息化", "IT运营部", 128.6201, 135, 7],
                ["信息化", "IT运营部", 128.6201, 28, 6],
                ["信息化", "IT运营部", 128.6201, 174, 5],
                ["信息化", "IT运营部", 128.6201, 49, 4],
                ["信息化", "IT运营部", 128.6201, 48, 3],
                ["信息化", "IT运营部", 128.6201, 157, 2],
                ["信息化", "IT运营部", 128.6201, 159, 1],
            ],
            columns=["投资类型", "单位x", "项目总投资合计", "value", "月份"],
        )

        file = Path("test_result.html")

        data = pbi.set_source(df)

        # drill down mutil series
        opts_bar = charts.make_bar(
            data, x="单位x", y="项目总投资合计", color="投资类型"
        ).hover_filter("color", data, "投资类型")
        opts_line = charts.make_line(data, x="月份", y="value", color="单位x")

        pbi.add_echart(opts_bar).set_debugTag("bar")
        pbi.add_echart(opts_line).set_debugTag("line")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    # @pytest.fixture
    # def page(self, page: Page, file_url):
    #     page.goto(file_url)
    #     return page

    @pytest.fixture
    def file_url_mutil_series(self):
        df = pd.DataFrame(
            [
                ["c1", "n1", 10, 1, 30, 69],
                ["c1", "n2", 20, 2, 62, 91],
                ["c1", "n3", 30, 3, 86, 98],
                ["c1", "n4", 40, 4, 18, 1],
                ["c1", "n5", 50, 5, 82, 97],
                ["c1", "n6", 60, 6, 41, 77],
                ["c1", "n7", 70, 7, 67, 24],
                ["c1", "n8", 80, 8, 72, 73],
                ["c2", "n1", 90, 1, 56, 77],
                ["c2", "n2", 100, 2, 46, 81],
                ["c2", "n3", 110, 3, 38, 27],
                ["c2", "n4", 120, 4, 30, 52],
                ["c2", "n5", 130, 5, 55, 53],
                ["c2", "n6", 140, 6, 85, 53],
                ["c2", "n7", 150, 7, 4, 25],
                ["c2", "n8", 160, 8, 84, 11],
                ["c2", "n9", 170, 9, 80, 80],
                ["c2", "n10", 180, 10, 13, 87],
                ["c2", "n11", 190, 11, 17, 33],
                ["c2", "n12", 200, 12, 8, 29],
            ],
            columns=["cat", "name", "value", "month", "p1", "p2"],
        )

        data = pbi.set_source(df)

        # drill down mutil series
        opts_bar = charts.make_bar(data, x="name", y="value", color="cat").hover_filter(
            "color", data, "cat"
        )
        opts_line = charts.make_line(data, x="name", y="value", color="cat")

        pbi.add_echart(opts_bar).set_debugTag("bar")
        pbi.add_echart(opts_line).set_debugTag("line")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    def test_one_series_no_errors(self, page: Page, file_url_one_series):
        page.goto(file_url_one_series)

        chart = utils.PageEChart(page, "bar").scroll2show()

        chart.hover_series(x_value="IT营业部", y_value=60)

        errs = utils.err_count(page)
        assert errs == 0

    def test_mutil_series_no_errors(self, page: Page, file_url_mutil_series):
        page.goto(file_url_mutil_series)

        bar_chart = utils.PageEChart(page, "bar").scroll2show()
        bar_chart.hover_series(
            x_value="n4", y_value=180, color="c2", offset_x=5, offset_y=-5
        )

        assert utils.err_count(page) == 0

        line_chart = utils.PageEChart(page, "line").scroll2show()

        opts = line_chart.get_options()
        assert opts["xAxis"][0]["data"] == [
            "n1",
            "n10",
            "n11",
            "n12",
            "n2",
            "n3",
            "n4",
            "n5",
            "n6",
            "n7",
            "n8",
            "n9",
        ]

        assert opts["series"][0]["data"] == [
            ["n1", 10],
            ["n2", 20],
            ["n3", 30],
            ["n4", 40],
            ["n5", 50],
            ["n6", 60],
            ["n7", 70],
            ["n8", 80],
        ]


class Test_click_filter:
    @pytest.fixture
    def page_file(self):
        df = pd.DataFrame(
            [
                {"colA": "1日", "colB": "A1", "value": 10},
                {"colA": "1日", "colB": "A2", "value": 10},
                {"colA": "2日", "colB": "A3", "value": 90},
                {"colA": "2日", "colB": "A4", "value": 90},
            ]
        )

        data = pbi.set_source(df)

        bar1 = pbi.easy_echarts.make_bar(data, x="colA", y="value")
        bar2 = pbi.easy_echarts.make_bar(data, x="colB", y="value")
        pbi.add_echart(bar1 + bar2).set_debugTag("bar")

        bar2.click_filter("x", data, "colB")

        pbi.add_table(data).set_debugTag("table")

        file = Path("test_result.html")
        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    def test_level2_backto_level1_clear_filters(self, page: Page, page_file):
        page.goto(page_file)

        chart = utils.PageEChart(page, "bar").scroll2show()
        table = utils.PageTable(page, "table")

        chart.click_series(x_value="2日", y_value=80)

        page.wait_for_timeout(1000)
        # level 2
        chart.click_series(x_value="A4", y_value=80)

        assert table.get_rows().count() == 1

        chart.click_back_to_button()
        page.wait_for_timeout(1000)

        assert table.get_rows().count() == 2


class Test_drill:
    @pytest.fixture
    def data_df(self):
        df = pd.DataFrame(
            [
                ["广东省", "广州市", "荔湾区"],
                ["广东省", "广州市", "海珠区"],
                ["广东省", "广州市", "白云区"],
                ["广东省", "深圳市", "南山区"],
                ["广东省", "深圳市", "盐田区"],
                ["广东省", "深圳市", "福田区"],
                ["湖南省", "长沙市", "芙蓉区"],
                ["湖南省", "长沙市", "天心区"],
                ["湖南省", "株洲市", "石峰"],
                ["湖南省", "株洲市", "渌口"],
            ],
            columns=list("省市区"),
        )

        df["value"] = range(1, len(df) + 1)
        return df

    @pytest.fixture
    def file_url(self, data_df: pd.DataFrame):
        file = Path("test_result.html")

        data = pbi.set_source(data_df)

        # drill down mutil series
        opts_bar = charts.make_bar(data, x="省", y="value", color="市").reverse_axis()
        opts_pie = opts = charts.make_pie(data, name="省", value="value")
        opts_line = charts.make_line(data, x="区", y="value")

        opts = opts_bar + opts_pie + opts_line

        pbi.add_echart(opts).set_debugTag("drill_mutil_series")

        # drill down one series
        opts_bar = charts.make_bar(data, x="省", y="value").reverse_axis()
        opts_pie = opts = charts.make_pie(data, name="省", value="value")
        opts_line = charts.make_line(data, x="区", y="value")

        opts = opts_bar + opts_pie + opts_line

        pbi.add_echart(opts).set_debugTag("drill_one_series")

        pbi.to_html(file)
        file_url = f"file:///{file.absolute()}"
        yield file_url

    @pytest.fixture
    def page(self, page: Page, file_url):
        page.goto(file_url)
        return page

    def test_no_err(self, page: Page):
        errs = utils.err_count(page)
        assert errs == 0

        # page.pause()

    def test_drill_down_one_series(self, page: Page):
        pass
        chart = utils.PageEChart(page, "drill_one_series").scroll2show()

        chart.click_series(2, "广东省")

        # page.wait_for_timeout(1000)

        errs = utils.err_count(page)
        assert errs == 0

        opts = chart.get_options()
        assert opts["series"][0]["type"] == "pie"

        # page.pause()

    def test_drill_down_mutil_series(self, page: Page):
        pass
        # chart = utils.PageBarEChart(page, "drill_mutil_series").scroll2show()

        # chart.hover_series(2, "湖南省", color="株洲市")

        # page.pause()

        # page.wait_for_timeout(1000)

        # errs = utils.err_count(page)
        # assert errs == 0

        # opts = chart.get_options()
        # assert opts["series"][0]["type"] == "pie"

        # page.pause()
